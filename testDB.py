import sqlite3

connection = sqlite3.connect('app.sqlite')
cursor = connection.cursor()

while True:
    command = input('>>> ')
    if command == 'exit':
        break
    else:
        try:
            if command.lower().startswith('select'):
                result = cursor.execute(command)
                for row in result:
                    print(row)
            else:
                cursor.execute(command)
        except sqlite3.OperationalError:
            print('Something went wrong...')
            

connection.commit()
connection.close()
