from flask import Flask, render_template, send_from_directory, request
import time
import posts

app = Flask('app')

@app.route('/static/<path:path>')
def static_files(path):
    return send_from_directory('static', path)

@app.route('/')
def landing_page():
    posts.log_request(page='landing', time=time.time(), client_ip=request.remote_addr)
    return render_template('landing.html')

@app.route('/about/')
def aboutus():
    posts.log_request(page='about', time=time.time(), client_ip=request.remote_addr)
    return render_template('about.html')

@app.route('/blog/')
def blog():
    posts.log_request(page='blog', time=time.time(), client_ip=request.remote_addr)
    all_posts = posts.get_posts()
    display_posts = []
    for row in all_posts:
        display_posts.append(row)
    ordering = request.args.get('ord')
    if ordering == 'reverse':
        display_posts.reverse()
    return render_template('blog.html', posts=display_posts)

@app.route('/blog/<int:post_row_id>')
def post_view(post_row_id):
    posts.log_request(page='post_{0}'.format(post_row_id), time=time.time(), client_ip=request.remote_addr)
    post = posts.get_single_post(post_row_id)
    return render_template('post-single-view.html', post=post)


@app.route('/contact/', methods=['GET', 'POST'])
def contact_page():
    if request.method == 'POST':
        m = request.form.get('message')
        print(m)
        return render_template('contact.html')
    else:
        return render_template('contact.html')

app.run(host='0.0.0.0', port=8000)
