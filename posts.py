import sqlite3

def get_posts():
        
    connection = sqlite3.connect('app.sqlite')
    cursor = connection.cursor()

    create_table = '''
        CREATE TABLE IF NOT EXISTS posts (
            row_id INTEGER PRIMARY KEY AUTOINCREMENT,
            title VARCHAR NOT NULL,
            content LONGTEXT
        )
    '''
    cursor.execute(create_table)

    select = 'SELECT * FROM posts'

    result = cursor.execute(select)

    return result
    


def get_single_post(post_id):
        
    connection = sqlite3.connect('app.sqlite')
    cursor = connection.cursor()

    create_table = '''
        CREATE TABLE IF NOT EXISTS posts (
            row_id INTEGER PRIMARY KEY AUTOINCREMENT,
            title VARCHAR NOT NULL,
            content LONGTEXT
        )
    '''
    cursor.execute(create_table)

    select = 'SELECT * FROM posts WHERE row_id={0}'.format(post_id)

    result = cursor.execute(select).fetchone()

    return result
    
def log_request(page='', time=0, client_ip=''):
    connection = sqlite3.connect('app.sqlite')
    cursor = connection.cursor()

    create_table = '''
        CREATE TABLE IF NOT EXISTS metrica (
            row_id INTEGER PRIMARY KEY AUTOINCREMENT,
            page VARCHAR NOT NULL,
            time REAL NOT NULL,
            client_ip VARCHAR
        )
    '''
    cursor.execute(create_table)

    insert = '''
        INSERT INTO metrica (page, time, client_ip)
        VALUES ("{0}", {1}, "{2}")
    '''.format(page, time, client_ip)

    cursor.execute(insert)

    connection.commit()
    connection.close()
